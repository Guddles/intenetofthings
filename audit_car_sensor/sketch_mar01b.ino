#define PIN_TRIG 12
#define PIN_ECHO 13
#define pin_vcc 11
#define dir_1 4
#define speed_1 5
#define speed_2 6
#define dir_2 7
long duration, cm;
//
// SHARP IR sensor testing
//
int IRpin = 0;                                    // аналоговый пин для подключения выхода Vo сенсора
void setup() {
  Serial.begin(9600);  
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT);
  pinMode(pin_vcc, OUTPUT);
  pinMode(dir_1, OUTPUT);
  pinMode(dir_2, OUTPUT);
  pinMode(speed_1, OUTPUT);
  pinMode(speed_2, OUTPUT);
  
  
  digitalWrite(pin_vcc, HIGH);// старт последовательного порта
  digitalWrite(dir_1, LOW);
  analogWrite(speed_1, 255);
  digitalWrite(dir_2, HIGH );
  analogWrite(speed_2, 128);
//  digitalWrite(m1, LOW);
}
void loop() {
  // 5V/1024 = 0.0048828125
  float volts = analogRead(IRpin)*0.0048828125;   // считываем значение сенсора и переводим в напряжение
  float distance = 65*pow(volts, -1.10);          // worked out from graph 65 = theretical distance / (1/Volts)S - luckylarry.co.uk
  Serial.println(distance); 
  Serial.print("Sharp");// выдаём в порт
  delay(100);    
  digitalWrite(PIN_TRIG, LOW);
  delayMicroseconds(5);
  digitalWrite(PIN_TRIG, HIGH);

  // Выставив высокий уровень сигнала, ждем около 10 микросекунд. В этот момент датчик будет посылать сигналы с частотой 40 КГц.
  delayMicroseconds(10);
  digitalWrite(PIN_TRIG, LOW);

  //  Время задержки акустического сигнала на эхолокаторе.
  duration = pulseIn(PIN_ECHO, HIGH);

  // Теперь осталось преобразовать время в расстояние
  cm = (duration / 2) / 29.1;

  Serial.print("Расстояние до объекта: ");
  Serial.print(cm);
  Serial.println(" см.");


  delay(250);
}
