#define door_pin = 10
bool unlocked = false;


void setup() {
  Serial.begin(9600);
  pinMode(door_pin, OUTPUT);
  

}

void loop() {
  update_state();
  do_action();
}

void update_state(){
  bool coin = get_coin();
  bool push = get_push();
  if (coin == true){
    unlocked = true;
  }
  if (push == true){
    unlocked = false;
  }
}

void do_action(){
  if(unlocked){
    digitalWrite(door_pin, HIGH);
  }
  else{
    digitalWrite(door_pin, LOW);
  }
}
