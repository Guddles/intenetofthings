#define led_pin 13

long _millis = 0;

void setup(){
  Serial.begin(9600);
  cli();
  TCCR1A = 0;
  TCCR1B = 0;

  OCR1A = 15624;

  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS10);

  TIMSK1 |= (1 << OCIE1A);

  sei();
}

ISR(TIMER1_COMPA_vect){
  _millis++;
}

long NewMillis(){
  return _millis;
}

void loop(){
  Serial.print(NewMillis());
  Serial.println();  
}
