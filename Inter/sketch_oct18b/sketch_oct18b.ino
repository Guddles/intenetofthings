#define led_pin 3
#define btn_pin 6
bool state = LOW;

void setup() {
  Serial.begin(9600);
  pinMode(btn_pin, INPUT_PULLUP);
  pinMode(led_pin, OUTPUT);
  int int_number = digitalPinToInterrupt(btn_pin);
  attachInterrupt(0, _blink, RISING);

}

void loop() {
//  int val = digitalRead(btn_pin);
  
  digitalWrite(led_pin, state);
  
  
}

void _blink(){
  state = !state;
}
