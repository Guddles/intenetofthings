#define led_pin 6

void setup() {
  Serial.begin(9600);
  while (!Serial){
    
  }
  pinMode(led_pin, OUTPUT);
}

void loop() {
  while(Serial.available() > 0){
    int message = Serial.readString().toInt();
    switch(message) {
      case 1:
        digitalWrite(led_pin, HIGH);
      break;
      case 2:
        digitalWrite(led_pin, LOW);
      break;
    }
  }
}

