import paho.mqtt.client as mqtt_client
import random
import time


broker = "broker.emqx.io"

client = mqtt_client.Client(f"lab_{random.randint(10000, 99999)}")


client.connect(broker)
# client.publish('lab/room1/sensor' ,random.randint(100,999))
while (True):
    client.publish('lab/room1/sensor', "ПОВЕСТКА В СООТВЕТСТВИИ С ФЕДЕРАЛЬНЫМ ЗАКОНОМ О ВОИНСКОЙ ОБЯЗАННОСТИ И ВОЕННОЙ СЛУЖБЕ ВЫ ПОДЛЕЖИТЕ ПРИЗЫВУ НА ВОЕННУЮ СЛУЖБУ И ОБЯЗАНЫ 23 ФЕВРАЛЯ 2008 Г К 12 (ЗАМЕНЯЕТСЯ) ПРИБЫТЬ В ВОЕННЫЙ КОМИССАРИАТ ПО АДРЕСУ: УЛ. СЕВЕРОЗВЕЗДНАЯ Д. 12, КОМНАТА 54 (ЗАМЕНЯЕТСЯ) ДЛЯ ПРОВЕДЕНИЯ МЕРОПРИЯТИЙ, СВЯЗАННЫХ С ПРИЗЫВОМ НА ВОЕННУЮ СЛУЖБУ. ПРИ СЕБЕ ИМЕТЬ: ПАСПОРТ (ДОКУМЕНТ, УДОСТОВЕРЯЮЩИЙ ЛИЧНОСТЬ), ХАРАКТЕРИСТИКУ С МЕСТА РАБОТЫ (УЧЕБЫ) В 2Х ЭКЗЕМПЛЯРАХ, ФОТО 3Х4 – 2 ШТ., РЕЗУЛЬТАТЫ АНАЛИЗА КРОВИ, МОЧИ (ОБЩИЙ), ФЛЮОРОГРАФИЮ (СНИМОК), ЭКГ СЕРДЦА В ПОКОЕ И С НАГРУЗКОЙ, АМБУЛАТОРНУЮ КАРТУ (ДРУГИЕ ДОКУМЕНТЫ, ХАРАКТЕРИЗУЮЩИЕ ВАШЕ СОСТОЯНИЕ ЗДОРОВЬЯ), РЕНТГЕНОЛОГИЧЕСКИЕ СНИМКИ, КСЕРОКОПИЯ СПРАВКИ ВТЭК (ЕСЛИ ИНВАЛИД), КСЕРОКОПИЯ ДИПЛОМА ОБ ОБРАЗОВАНИИ (АТТЕСТАТА), СПРАВКА С МЕСТА УЧЕБЫ (ФОРМА № 26). ВОЕННЫЙ КОМИССАР РАЙОНА СТАРШИЙ СЕРЖАНТ")
client.disconnect()