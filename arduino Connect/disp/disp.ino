#define G 3 // подключение сегмента 3 к PIN3
#define F 4 // подключение сегмента 4 к PIN4
#define A 5 // подключение сегмента 5 к PIN5
#define B 6 // подключение сегмента 6 к PIN6
#define E 7 // подключение сегмента 7 к PIN7
#define D 8 // подключение сегмента 8 к PIN8
#define C 9 // подключение сегмента 9 к PIN9
#define DP 10 // подключение сегмента 10 к PIN10

void setup(){
  Serial.begin(9600);
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(C, OUTPUT);
  pinMode(D, OUTPUT);
  pinMode(E, OUTPUT);
  pinMode(F, OUTPUT);
  pinMode(G, OUTPUT);
  pinMode(DP, OUTPUT);
}

void loop(){
  digitalWrite(A, HIGH);
  digitalWrite(F, HIGH);
  digitalWrite(B, HIGH);
  digitalWrite(G, HIGH);
}
