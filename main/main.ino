#define sensor_pin A2
#define led_pin 13

void setup(void){
  Serial.begin(9600);
  pinMode(led_pin, OUTPUT);
   
}

void check_serial(){
  while(Serial.available()>0){
    char message = Serial.read();
    if(message == 'u'){
      digitalWrite(led_pin, HIGH);
      Serial.println("Led is on");
    }
    else if (message == 'd'){
      digitalWrite(led_pin, HIGH);
      Serial.println("Led is off");
    }else if(message == 's'){
      int val = analogRead(sensor_pin);
      Serial.write(val);
      Serial.println(val);
    }
  }
}


void loop(){
  int val = analogRead(sensor_pin);
  check_serial();
//  Serial.println(val);
}
