#define sensor A0
#define led_pin 13

int value = 0;

void setup() {
  Serial.begin(9600);
  pinMode(led_pin, OUTPUT);
}

void loop(){  
  int value = analogRead(sensor);
  int val = map(value(0, 1000, 10, 1));
  while (val >=1 && val<=5){
    digitalWrite(led_pin, HIGH);
    delay(1500);
  }
  while (val<=6){
    digitalWrite(led_pin, HIGH);
    delay(500);
  }
  
//  Serial.println(analogRead(sensor));
//  Serial.println(value);
}
