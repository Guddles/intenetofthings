//  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
#include "WIFI.h"
#include "server.h"
#include <SoftwareSerial.h>
#define PIN_TRIG 13 // ПИН D7
#define PIN_ECHO 12 // ПИН D6
#define analog_pin A0 // ПИН A0
#define led_pin 4 // ПИН D2

long duration, cm;
extern bool flag;
int s;
SoftwareSerial mySerial(9, 10);
void setup() {
  Serial.begin (9600); 
  while (!Serial) {}
  mySerial.begin (9600);
  while (!mySerial) {}
  WiFi_init(true);
  server_init();
  
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT); // Определяем вывод
  pinMode(analog_pin, INPUT);
  pinMode(led_pin, OUTPUT);
  
  

}
void loop() {
  server.handleClient();
  if (flag) {
    digitalWrite(PIN_TRIG, LOW);
    delayMicroseconds(5);
    digitalWrite(PIN_TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(PIN_TRIG, LOW);
    duration = pulseIn(PIN_ECHO, HIGH);
    cm = (duration / 2) / 29.1;
    String ans = "1"+String(cm) ;
    Serial.println(ans);
//    mySerial.println(ans);
    delay(250);
  } else {
    int value = analogRead(analog_pin);
    Serial.println("0" + String(value));
//    mySerial.println("0" + String(value));
    delay(250);
  }

  while(Serial.available()){
    s = Serial.read();
  }
  if(s == '1'){
    digitalWrite(led_pin, HIGH);
  }
  else if(s == '0'){
    digitalWrite(led_pin, LOW);
  }
  

  
}
