int ledpins[] = {2,3,4,5};
int groundpins[] = {6,7};
void setup ()
{
  for(int i = 0; i < 4; i++)
  {       
      pinMode(ledpins[i],OUTPUT);
  }                         
  for (int i= 0; i<2; i++)
  {pinMode(groundpins[i],OUTPUT);}

}

void loop()
{
  twist();
  //drop();
  //diagonal();
}

void twist(){
  int delaytime=100;
  led1_on();
  delay(delaytime);
  led1_off();
  led2_on();
  delay(delaytime);
  led2_off();
  led3_on();
  delay(delaytime);
  led3_off();
  led4_on();
  delay(delaytime);
  led4_off();
  //нижние
  led5_on();
  delay(delaytime);
  led5_off();
  led6_on();
  delay(delaytime);
  led6_off();
  led7_on();
  delay(delaytime);
  led7_off();
  led8_on();
  delay(delaytime);
  led8_off();
}

void drop(){
  int delaytime = 75;
  led1_on();
  delay(delaytime);
  led1_off();
  led5_on();
  delay(delaytime);
  led5_off();

  led2_on();
  delay(delaytime);
  led2_off();
  led6_on();
  delay(delaytime);
  led6_off();
 
  led3_on();
  delay(delaytime);
  led3_off();
  led7_on();
  delay(delaytime);
  led7_off();

  led4_on();
  delay(delaytime);
  led4_off();
  led8_on();
  delay(delaytime);
  led8_off();
 
  //second time down
  led1_on();
  delay(delaytime);
  led1_off();
  led5_on();
  delay(delaytime);
  led5_off();

  led2_on();
  delay(delaytime);
  led2_off();
  led6_on();
  delay(delaytime);
  led6_off();
 
  led3_on();
  delay(delaytime);
  led3_off();
  led7_on();
  delay(delaytime);
  led7_off();

  led4_on();
  delay(delaytime);
  led4_off();
  led8_on();
  delay(delaytime);
  led8_off();
}

void diagonal(){
  int delaytime= 10;
  led1_on();
  delay(delaytime);
  led1_off();
  led8_on();
  delay(delaytime);
  led8_off();
}


void led1_on() {
  digitalWrite(groundpins[0], HIGH);
  digitalWrite(ledpins[0], HIGH);
}
void led1_off() {
  digitalWrite(groundpins[0], LOW);
  digitalWrite(ledpins[0], LOW);
}
void led2_on() {
  digitalWrite(groundpins[0], HIGH);
  digitalWrite(ledpins[1], HIGH);
}
void led2_off() {
  digitalWrite(groundpins[0], LOW);
  digitalWrite(ledpins[1], LOW);
}
void led3_on() {
  digitalWrite(groundpins[0], HIGH);
  digitalWrite(ledpins[2], HIGH);
}
void led3_off() {
  digitalWrite(groundpins[0], LOW);
  digitalWrite(ledpins[2], LOW);
}
void led4_on() {
  digitalWrite(groundpins[0], HIGH);
  digitalWrite(ledpins[3], HIGH);
}
void led4_off() {
  digitalWrite(groundpins[0], LOW);
  digitalWrite(ledpins[3], LOW);
}
void led5_on() {
  digitalWrite(groundpins[1], HIGH);
  digitalWrite(ledpins[0], HIGH);
}
void led5_off() {
  digitalWrite(groundpins[1], LOW);
  digitalWrite(ledpins[0], LOW);
}
void led6_on() {
  digitalWrite(groundpins[1], HIGH);
  digitalWrite(ledpins[1], HIGH);
}
void led6_off() {
  digitalWrite(groundpins[1], LOW);
  digitalWrite(ledpins[1], LOW);
}
void led7_on() {
  digitalWrite(groundpins[1], HIGH);
  digitalWrite(ledpins[2], HIGH);
}
void led7_off() {
  digitalWrite(groundpins[1], LOW);
  digitalWrite(ledpins[2], LOW);
}
void led8_on() {
  digitalWrite(groundpins[1], HIGH);
  digitalWrite(ledpins[3], HIGH);
}
void led8_off() {
  digitalWrite(groundpins[1], LOW);
  digitalWrite(ledpins[3], LOW);
}
