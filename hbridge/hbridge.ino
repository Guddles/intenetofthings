
int antiSwitchP = 8;
int antiSwitchN = 9;

int cwSwitchP = 7;
int cwSwitchN = 6;

void setup() {
   
   pinMode(antiSwitchP, OUTPUT);      
   pinMode(antiSwitchN, OUTPUT); 
   pinMode(cwSwitchP, OUTPUT);  
   pinMode(cwSwitchN, OUTPUT); 
}

void loop(){
     digitalWrite(cwSwitchP, LOW);
     digitalWrite(cwSwitchN, LOW);
     
     
     digitalWrite(antiSwitchP, HIGH);  
     digitalWrite(antiSwitchN, HIGH); 
     delay(1000);
     
     
     digitalWrite(antiSwitchP, LOW); 
     digitalWrite(antiSwitchN, LOW); 
     delay(1000);
     
     
     digitalWrite(cwSwitchP, HIGH); 
     digitalWrite(cwSwitchN, HIGH); 
     delay(1000);
     
     
     digitalWrite(cwSwitchP, LOW); 
     digitalWrite(cwSwitchN, LOW);  
     delay(1000);

}
//void switch_drive(int dir = 0, int speed = 255){
//  if (dir){
//    
//  }
//}
